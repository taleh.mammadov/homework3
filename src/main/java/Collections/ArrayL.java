package Collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class ArrayL {
    public static void main(String[] args) {
       /* ArrayList<String> name = new ArrayList<String>();
        name.add("Peri");
        name.add("Cavad");
        name.add("Arif");
        name.add("Arif");
        System.out.println(name.get(0));
        for (String ad: name) {
            System.out.println(ad);
        }
        List<String> name2 = new ArrayList<String>();

        HashSet<String> mySet = new HashSet<String>();
        mySet.add("Man");
        mySet.add("Woman");
        mySet.add("Woman");
        mySet.add("Girl");
        mySet.add("Boy");
*/
        Student s1 = new Student(1,"Cavad","Mammadov");
        Student s2 = new Student(2,"Mavad","Ammad");
        Student s3 = new Student(3,"Avad","Sammad");
        Student s4 = new Student(3,"Avad","Sammad");
        Student s5 = new Student(5,"Qavad","Semmed");
        HashSet<Student> telebeler = new HashSet<Student>();
        telebeler.add(s1);
        telebeler.add(s2);
        telebeler.add(s3);
        telebeler.add(s4);
        telebeler.add(s5);

        for (Student telebe: telebeler) {
            System.out.println(telebe.number+ " " +telebe.name);
            System.out.println(telebe.hashCode());
        }

        HashMap<String,Student> hMap = new HashMap<String,Student>();
        hMap.put(String.valueOf(s1.number),s1);
        hMap.put(String.valueOf(s2.number),s2);
        hMap.put(String.valueOf(s3.number),s3);
        hMap.put(String.valueOf(s4.number),s4);
        hMap.put(String.valueOf(s5.number),s5);

        System.out.println(hMap.get("2"));
        
    }
}
